FROM node:10-alpine as builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm install ngx-bootstrap --save
RUN npm install bootstrap@3.3.7
RUN npm add ngx-bootstrap  --component carousel
RUN $(npm bin)/ng build

# stage 2
FROM nginx:alpine
COPY --from=builder /app/dist/dow /usr/share/nginx/html
